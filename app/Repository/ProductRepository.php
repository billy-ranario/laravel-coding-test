<?php

namespace App\Repository;

use App\Dtos\ProductDto;
use App\Models\Product;
use Illuminate\Support\Facades\ {
    DB,
    Log
};

class ProductRepository
{
    /**
     * get products
     * @param ProductDto $productDto
     * @return Product | null
     */
    public function get(ProductDto $productDto): Product | null
    {
        return Product::query()
            ->orderBy($productDto->getSortColumn(), $productDto->getSortDirection())
            ->paginate($productDto->getPerPage());
    }

    /**
     * get product by id
     * @param int $id
     * @return Product | null
     */
    public function getById(int $id): Product | null
    {
        return Product::find($id);
    }

    /**
     * create product
     * @param ProductDto $productDto
     * @return Product | bool
     */
    public function create(ProductDto $productDto): Product | bool
    {
        try {
            DB::beginTransaction();
            $product = Product::create([
                'product_name' => $productDto->getProductName(),
                'product_description' => $productDto->getProductDescription() ?? null,
                'product_filename' => null, // TODO
                'product_price' => $productDto->getProductPrice(),
                'quantity' => $productDto->getProductQuantity(),
            ]);
            DB::commit();

            return $product;
        } catch (\Throwable $th) {
            Log::error(
                'File: ' . $th->getFile() . ' at line ' . $th->getLine() . '\n'. 
                'Message: ' . $th->getMessage()
            );
            return false;
        }
    }

    /**
     * update product
     * @param ProductDto $productDto
     * @return Product | bool
     */
    public function update(ProductDto $productDto): Product | bool
    {
        try {
            DB::beginTransaction();
            $product = Product::find($productDto->getProductId());
            
            if (!empty($productDto->getProductName())) {
                $product->product_name = $productDto->getProductName();
            }
            if (!empty($productDto->getProductDescription())) {
                $product->product_description = $productDto->getProductDescription();
            }
            if (!empty($productDto->getProductPrice())) {
                $product->product_price = $productDto->getProductPrice();
            }
            if (!empty($productDto->getProductQuantity())) {
                $product->quantity = $productDto->getProductQuantity();
            }

            $product->save();

            DB::commit();

            return $product;
        } catch (\Throwable $th) {
            Log::error(
                'File: ' . $th->getFile() . ' at line ' . $th->getLine() . '\n'. 
                'Message: ' . $th->getMessage()
            );
            return false;
        }
    }

    /**
     * delete product
     * @param int $id
     * @return bool
     */
    public function delete(int $id)
    {
        return Product::where('id', $id)->delete();
    }

}
