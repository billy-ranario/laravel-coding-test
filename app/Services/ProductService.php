<?php

namespace App\Services;

use App\Core\ServiceResponse;
use App\Dtos\ProductDto;
use App\Repository\ProductRepository;

class ProductService
{
    /**
     * @var ProductRepository $productRepository
     */
    private ProductRepository $productRepository;

    /**
     * ProductService constructor.
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Get paginated products 
     * @param ProductDto $productDto
     * @return ServiceResponse
     */
    public function get(ProductDto $productDto): ServiceResponse
    {
        if ($products = $this->productRepository->get($productDto)) {
            return ServiceResponse::success('', $products);
        }

        return ServiceResponse::error('Unable to fetch products');
    }

    /**
     * Get product by Id 
     * @param ProductDto $productDto
     * @return ServiceResponse
     */
    public function getById(ProductDto $productDto): ServiceResponse
    {
        if ($products = $this->productRepository->getById($productDto->getProductId())) {
            return ServiceResponse::success('', $products);
        }

        return ServiceResponse::error('Unable to fetch products');
    }

    /**
     * Create product 
     * @param ProductDto $productDto
     * @return ServiceResponse
     */
    public function createProduct(ProductDto $productDto): ServiceResponse
    {
        if ($products = $this->productRepository->create($productDto)) {
            return ServiceResponse::success('Product successfully create.', $products);
        }

        return ServiceResponse::error('Unable to create product');
    }

    /**
     * Update product 
     * @param ProductDto $productDto
     * @return ServiceResponse
     */
    public function updateProduct(ProductDto $productDto): ServiceResponse
    {
        if ($products = $this->productRepository->update($productDto)) {
            return ServiceResponse::success('Product successfully updated.', $products);
        }

        return ServiceResponse::error('Unable to update product');
    }

    /**
     * Delete product 
     * @param ProductDto $productDto
     * @return ServiceResponse
     */
    public function deleteProduct(ProductDto $productDto): ServiceResponse
    {
        if ($products = $this->productRepository->delete($productDto->getProductId())) {
            return ServiceResponse::success('Product successfully renived');
        }

        return ServiceResponse::error('Unable to delete product');
    }
}
