<?php

namespace App\Dtos;

class ProductDto {
    /**
     * @var int $product_id
     * @var string $product_name
     * @var string $product_description
     * @var int $product_price
     * @var int $quantity
     * @var int $perPage
     * @var string sortColumn
     * @var string sortDirection
     */
    private int $product_id;
    private string $product_name;
    private string $product_description;
    private int $product_price;
    private int $quantity;
    private int $perPage;
    private string $sortColumn;
    private string $sortDirection;

    /**
     * Get the product id
     * @return int
     */
    public function getProductId(): int
    {
        return $this->product_id;
    }

    /**
     * Get the value of product_name
     * @return string
     */
    public function getProductName(): string
    {
        return $this->product_name;
    }

    /**
     * Get the value of product_description
     * @return string
     */
    public function getProductDescription(): string
    {
        return $this->product_description;
    }

    /**
     * Get the value of product_price
     * @return int
     */
    public function getProductPrice(): int
    {
        return $this->product_price;
    }

    /**
     * Get the value of quantity
     * @return int
     */
    public function getProductQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * Get the numbers per page
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * Get the sort column
     * @return string
     */
    public function getSortColumn(): string
    {
        return $this->sortColumn;
    }

    /**
     * Get the sort direction
     * @return string
     */
    public function getSortDirection(): string
    {
        return $this->sortDirection;
    }

    /**
     * Set the product_name
     * @param string $product_name
     * @return void
     */
    public function setProductName(string $product_name): void
    {
        $this->product_name = $product_name;
    }

    /**
     * Set the product_id
     * @param int $product_id
     * @return void
     */
    public function setProductId(int $product_id): void
    {
        $this->product_id = $product_id;
    }

    /**
     * Set the product_description
     * @param string $product_description
     * @return void
     */
    public function setProductDescription(string $product_description): void
    {
        $this->product_description = $product_description;
    }

    /**
     * Set the product_price
     * @param int $product_price
     * @return void
     */
    public function setProductPrice(int $product_price): void
    {
        $this->product_price = $product_price;
    }

    /**
     * Set the quantity
     * @param int $quantity
     * @return void
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * Set the per page value
     * @param int $perPage
     * @return void
     */
    public function setPerPage(int $perPage): void
    {
        $this->perPage = $perPage;
    }

    /**
     * Set the sort column
     * @param string $sortColumn
     * @return void
     */
    public function setSortColumn(string $sortColumn): void
    {
        $this->sortColumn = $sortColumn;
    }

    /**
     * Set the sort direction
     * @param string $sortDirection
     * @return void
     */
    public function setSortDirection(string $sortDirection): void
    {
        $this->sortDirection = $sortDirection;
    }
}