<?php

namespace App\Http\Requests\Product;

use App\Dtos\ProductDto;
use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * Transform request to data transfer object.
     * @return ProductDto
     */
    public function toDto(): ProductDto
    {
        $productDto = new ProductDto();

        if (!empty($this->input('id'))) {
            $productDto->setProductId((int) $this->input('id'));
        }

        $productDto->setPerPage((int) $this->input('perPage'));
        $productDto->setSortColumn($this->input('sortColumn'));
        $productDto->setSortDirection($this->input('sortDirection'));

        return $productDto;
    }
}
