<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\{
    ProductRequest,
    ProductCreateRequest
};
use App\Services\ProductService;
use Illuminate\Http\Request;
use Spatie\FlareClient\Http\Response;

class ProductController extends Controller
{
    private ProductService $productService;
    /**
     * ProductController constructor
     * @param ProductService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(ProductRequest $request)
    {
        $serviceResponse = $this->productService->get($request->toDto());

        if ($serviceResponse->isSucces()) {
            return response()->json($serviceResponse->getData());
        }
        return response()->json(['error' => $serviceResponse->getMessage()], 400);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductCreateRequest $request)
    {
        $serviceResponse = $this->productService->createProduct($request->toDto());

        if ($serviceResponse->isSucces()) {
            return response()->json($serviceResponse->getData());
        }
        return response()->json(['error' => $serviceResponse->getMessage()], 400);
    }

    /**
     * Display the specified resource.
     */
    public function show(ProductRequest $request)
    {
        $serviceResponse = $this->productService->getById($request->toDto());

        if ($serviceResponse->isSucces()) {
            return response()->json($serviceResponse->getData());
        }
        return response()->json(['error' => $serviceResponse->getMessage()], 400);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
