<?php

namespace App\Http\Requests\Product;

use App\Dtos\ProductDto;
use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'productName' => ['required', 'string'],
            'productPrice' => ['required', 'string'],
            'quantity' => ['required', 'integer']
        ];
    }

    /**
     * Transform request to data transfer object.
     * @return ProductDto
     */
    public function toDto(): ProductDto
    {
        $productDto = new ProductDto();

        $productDto->setProductName($this->input('productName'));
        $productDto->setProductDescription($this->input('productDescription'));
        $productDto->setProductPrice($this->input('productPrice'));
        $productDto->setQuantity($this->input('quantity'));

        return $productDto;
    }
}
